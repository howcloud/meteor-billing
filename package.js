// NOTE: assumes HCConfig is defined globally with stripeSecretKey on server and stripePublishableKey on client
// TODO: make a way to handle subscription/reoccuring invoicing
//		 would work by automatically generating and paying invoices at certain points in time, terminating when told to terminate (ie when user unsubscribes from a billing agreement etc)

Package.describe({
	name: 'howcloud:billing',
	version: '0.1.0',
	summary: 'Exports the invoice and payment system for an app including collections for invoices',
});

Npm.depends({
	'stripe': '4.0.0',
});

Package.on_use(function (api) {
	
	/* Package Dependencies */

	api.use('underscore');
	api.use('ddp');
	api.use('mongo');
	api.use('meteor');
	api.use('livedata');
	api.use('jquery', {weak: true});

	api.use('meteorhacks:async');
	
	api.use('howcloud:collection-base');
	api.use('howcloud:users');
	api.use('howcloud:react-email', 'server');

	/* Add files */

	// Stripe

	api.add_files('lib/Stripe/Stripe_server.js', ['server']);
	api.add_files('lib/Stripe/Stripe_client.js', ['client']);

	api.export('deferUntilStripeLoaded', ['client']);

	// Invoices

	api.add_files('collections/invoices_shared.js', ['client', 'server']);
	api.add_files('collections/invoices_client.js', ['client']);
	api.add_files('collections/invoices_server.js', ['server']);

	// Users

	// api.add_files('collections/users_shared.js', ['client', 'server']);
	api.add_files('collections/users_client.js', ['client']);
	api.add_files('collections/users_server.js', ['server']);

	// Coupons

	api.add_files('collections/coupons_shared.js', ['client', 'server']);
	api.add_files('collections/coupons_server.js' , ['server']);

	/* Export */

	// api.export('Stripe', ['server']); // window.stripe will load up on client using our async loader
	api.export('Invoices', ['client', 'server']);
	api.export('Coupons', ['client', 'server']);
	// users is already exported in virtue of loading howcloud:users - we just extend it here with additional functionality

});