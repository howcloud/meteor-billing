/**

Invoices
Used to manage payment processing and recording of transactions
An abstraction which allows us to cleanly display the payment process flow on the client side
On the server side allows us to create hooks to update variable/run methods when payments are complete

**/

Invoices = Collections.init("invoices", {
	mixins: [Collections.mixins.user]
});

/*******************************************************************************************/

/** Collection **/

Invoices.extendCollection({

	/** Get **/

	getPaidInvoicesForUser: function (_user_id, projection) {
		return this.getPaidInvoicesForUser_cursor(_user_id).fetch();
	},
	getPaidInvoicesForUser_cursor: function (_user_id, projection) {
		return this.collection.find({
			user_id: _user_id,
			status: 'paid'
		});
	}

});

/*******************************************************************************************/

/** Document **/

Invoices.extendDocument({

	/** Totals **/

	total: function () {
		var total = 0;

		_.each(this.items, function (item) {
			total += item.price*item.quantity;
		});

		return total;
	},

	itemTotal: function (index) {
		return this.items[index].price * this.items[index].quantity;
	},

	/** Formatted Totals **/

	formattedTotal: function () {
		return formatMoney(this.total(), this.currency);
	},

	formattedItemTotal: function (index) {
		return formatMoney(this.itemTotal(index), this.currency);
	},

});