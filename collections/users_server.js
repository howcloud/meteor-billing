Users.extendDocument({

	_stripeCustomerData: function () {
		return {
			email: this.defaultEmail(),
			metadata: {
				name: this.profile && this.profile.name ? this.profile.name : ''
			}
		}
	},

	_hasStripeAccount: function () {
		return this.payment && this.payment.stripe && this.payment.stripe.customerId ? true : false;
	},
	_requireStripeCustomer: function () {
		if (!this._hasStripeAccount()) {
			if (!this._createStripeCustomer()) return false;
		}

		return true;
	},
	_createStripeCustomer: function () {
		var customer = Stripe.createCustomer(this._id, this._stripeCustomerData());
		if (!customer) return false;

		Users.collection.update(this._id, {
			$set: {'payment.stripe.customerId': customer.id}
		});

		// In place update necessary because we might call this when adding the first card, for example
		this.payment = this.payment || {};
		this.payment.stripe = this.payment.stripe || {};
		this.payment.stripe.customerId = customer.id;

		return true;
	},
	_updateStripeCustomer: function (_data) {
		if (this._hasStripeAccount()) return Stripe.upateCustomerData(this.payment.stripe.customerId, _data);
	},

	syncStripe: function () {
		return this._updateStripeCustomer(this._stripeCustomerData());
	},

	getDefaultStripeCard: function () {
		if (!this.payment || !this.payment.cards) return null;

		return _.find(this.payment.cards, function (card) {
			return card.type === 'stripe' && card.default ? true : false;
		});
	},
	getStripeCardById: function (cardId) {
		if (!this.payment || !this.payment.cards) return null;

		return _.find(this.payment.cards, function (card) {
			return card.type === 'stripe' && card.id === cardId ? true : false;
		});
	},
	addStripeCardFromToken: function (cardToken) {
		if (!this._requireStripeCustomer()) return false;

		var card = Stripe.createCard(this.payment.stripe.customerId, cardToken);
		if (!card) return false;

		var isDefault = false;
		if (!this.getDefaultStripeCard()) isDefault = true; // will be made default automatically by stripe anyway

		var _pushCard = {
			id: card.id,
			type: 'stripe',
			brand: card.brand,
			last4: card.last4,
			expMonth: card.exp_month,
			expYear: card.exp_year,
			funding: card.funding,
			default: isDefault,
		}

		Users.collection.update(this._id, {
			$push: {'payment.cards': _pushCard}
		});

		// In place update, might not even be necessary in this case, though
		this.payment = this.payment || {};
		this.payment.cards = this.payment.cards || [];
		this.payment.cards.push(_pushCard);

		return card.id;
	},
	removeStripeCard: function (cardId) {
		if (!this._requireStripeCustomer()) return false;

		var card = this.getStripeCardById(cardId);
		if (!card) return false;

		if (Stripe.deleteCard(this.payment.stripe.customerId, card.id)) return true;
		
		return false;
	},
	setStripeCardDefault: function (cardId) {
		// We don't need to make a call to Stripe to set this
		// We handle all payments from our system and so store the default here and then fetch that default to make a charge => no need to send data about which is default to Stripe
		// Stripe will alway assume first card added is the default

		if (!this._requireStripeCustomer()) return false;

		var card = this.getStripeCardById(cardId);
		if (!card) return false;

		var currDefault = this.getDefaultStripeCard();
		if (currDefault) {
			Users.collection.update({
				_id: this._id,
				'payment.cards': {
					id: currDefault.id
				}
			}, 
			{
				$set: {'payment.cards.$.default': false}
			});
		}

		Users.collection.update({
			_id: this._id,
			'payment.cards': {
				id: cardId
			}
		},
		{
			$set: {'payment.cards.$.default' : true}
		});

		return true;
	},

	chargeStripeCard: function (cardId, amount, options) {
		// options allow us to set currency, description for the transaction etc, see Stripe docs: https://stripe.com/docs/api#create_charge

		if (!this._requireStripeCustomer()) return false;

		var card;
		if (cardId) {
			card = this.getStripeCardById(cardId);
			if (!card) return false;
		} else {
			card = this.getDefaultStripeCard();
		}

		options = options || {};

		var charge = Stripe.doCharge(amount, card.id, this.payment.stripe.customerId, options);
		if (!charge) return false;

		return true;
	},



});

Meteor.methods({

	thisUser_addStripeCard: function (_cardToken) {
		var user = Meteor.user();
		if (!user) throw new Meteor.Error(404, "You must have an account to add a card to");

		return user.addStripeCardFromToken(_cardToken);
	},
	thisUser_removeStripeCard: function (_cardId) {
		var user = Meteor.user();
		if (!user) throw new Meteor.Error(404, "You must have an account to remove a card from");

		return user.removeStripeCard(_cardId);
	},

});