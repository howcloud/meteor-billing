/**

Coupons
Server side functionality 

**/

/*****************************************************************************/
/** Collection **/

Coupons.extendCollection({

	/** New **/

	new: function (_codename, _data) {
		if (!_codename) return null;
		_data = _data || {}

		return Coupons.collection.insert(_.extend({
			codename: [_codename],
			// data: {}, // inserts as an array when we put it in empty for some reason
			active: true
		}, _data));
	},

	newCouponWithRandomCodename: function (data, length) {
		length = length || 5;

		var codename;
		while (true) {
			codename = randString(length);
			if (!Coupons.getByCodename(codename)) break;
		}

		Coupons.new(codename, data);

		return codename;
	},

});

/*****************************************************************************/
/** Document **/

Coupons.extendDocument({

	// setUsed:
	// Records a use count for this coupon

	setUsed: function () {

		// Update collection

		Coupons.collection.update(this._id, {
			$inc: {
				'statistics.used': 1
			}
		});

		// In place update

		this.statistics = this.statistics || {}
		this.statistics.used++;

	},

});

/*****************************************************************************/
/** Publications **/

Coupons.publishFields = {
	statistics: 0
}

Meteor.publish("Coupon_byIdentifier", function (_identifier) {
	return Coupons.getByIdentifier_cursor(_identifier, {
		fields: Coupons.publishFields
	});
});