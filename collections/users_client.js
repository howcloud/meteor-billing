Users.extendDocument({

	/** Cards **/

	addStripeCard: function (cardToken, __callback) {
		if (Meteor.userId() != this._id) return false;

		Meteor.call("thisUser_addStripeCard", cardToken, __callback);
	},

	removeStripeCard: function (cardId, __callback) {
		if (Meteor.userId() != this._id) return false;

		Meteor.call("thisUser_removeStripeCard", cardId, __callback);
	},

});