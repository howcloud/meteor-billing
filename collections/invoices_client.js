/**

Invoices [Client Side]

**/

/** Document **/

Invoices.extendDocument({

	payWithCard: function (cardId, __callback) {
		Meteor.methods("invoice_charge", cardId, __callback);
	},

	cancel: function (__callback) {
		if (!this.allowCancel) return false;

		Meteor.methods("invoice_cancel", this._id, __callback);
	},

})