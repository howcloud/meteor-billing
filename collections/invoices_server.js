/**

Invoices [server side]

**/

/** Collection **/

Invoices.extendCollection({

	/** New **/

	new: function (_user_id, options) {
		options = options || {};

		var insert = {
			user_id: _user_id,
			type: options.type, 
			data: options.data, // used by the type to react to payment of the invoice, for example
			currency: options.currency ? options.currency : 'gbp',
			description: options.description,
			statement_description: options.statement_description,
			status: 'pending',
			createDate: new Date(),
			allowCancel: options.allowCancel === undefined ? true : options.allowCancel, // allow the user to cancel this invoice ? (ie if they want to back out of this purchase, by default = yes)
			/*items: [],*/
		}

		var invoice_id = Invoices.collection.insert(insert);

		return this.getById(invoice_id);
	},

	/** Invoice Types **/
	// Allows us to do custom setup of invoices and register callbacks on charge etc

	types: {},
	registerInvoiceType: function (codename, typeData) {
		/*
	
		typeData: {
			new: function (user_id, options), given options => create and return an invoice, if undefined then we use standard 
			onCharge: function (invoice) , passes an invoice when it is successfully charged
		}

		*/

		this.types[codename] = typeData;
	},
	newInvoiceOfType: function (codename, user_id, options) {
		var type = this.types[codename];
		options.type = codename;

		if (type.new) {
			return type.new(user_id, options);
		} else {
			return Invoices.new(user_id, options);
		}
	},

});

/*******************************************************************************************/

/** Document **/

Invoices.extendDocument({

	/** Items **/

	// note: prices should be in lowest currency unit (ie cents, pence etc) so £5.52 = 552

	addItem: function (_name, _price, options) {
		// options: {quantity, description}
		options = options || {}

		var item = {
			name: _name,
			description: options.description,
			price: _price,
			quantity: options.quantity ? options.quantity : 1
		}

		Invoices.collection.update(this._id, {
			$push: {items: item}
		});

		// In place update
		this.items = this.items || [];
		this.items.push(item);
	},

	/** Charge **/

	// charges the user's stripe cardId for the invoice [todo: abstract away from stripe card id]
	// if successful and the invoice is of a particular type - call the onCharge callback for that type

	charge: function (cardId) {
		if (this.status === 'paid') return false;

		var user = Users.getById(this.user_id);

		var charged = user.chargeStripeCard(cardId, this.total(), {
			currency: this.currency,
			description: "Invoice " + this._id,
			statement_description: this.statement_description
		});

		if (charged) {
			var _paidDate = new Date();
			Invoices.collection.update(this._id, {
				$set: {status: 'paid', paidDate: _paidDate}
			});

			this.status = 'paid';
			this.paidDate = _paidDate;

			if (this.type) {
				var type = Invoices.types[this.type];
				if (type && type.onCharge) {
					type.onCharge(this);
				}
			}

			/* Send Email Receipt */

			if (typeof Email_Invoice_Paid !== 'undefined') { // TODO: work out a way to 'register' this email with the package rather than relying on it being defined as a global variable at runtime
				var emailAddr = user.defaultEmail();
				if (emailAddr) {
					var email = new TemplateEmail(emailAddr, 'Payment Received');
					
					email.setTemplate(Email_Invoice_Paid, {
						invoice: this
					});

					email.send();
				}
			}

			/********************/

			return true;
		}

		return false;
	},

});

Meteor.methods({

	invoice_charge: function (_invoice_id, _cardId) {
		var invoice = Invoices.getById(_invoice_id);
		if (!invoice) throw new Meteor.Error(404, "Invoice not found");
		if (invoice.user_id != Meteor.userId()) throw new Meteor.Error(404, "Invoice not found");

		return invoice.charge(_cardId);
	},

	invoice_cancel: function (_invoice_id) {
		var invoice = Invoices.getById(_invoice_id);
		if (!invoice) throw new Meteor.Error(404, "Invoice not found");
		if (invoice.user_id != Meteor.userId()) throw new Meteor.Error(404, "Invoice not found");
		
		if (!invoice.allowCancel) throw new Meteor.Error(403, "You cannot cancel this invoice");

		Invoices.collection.remove(_invoice_id); // should be able to do invoices.delete() ???

		return true;
	},

});

/*******************************************************************************************/

/** Publish **/

Invoices.publishFields = {
	description: 1,
	user_id: 1,
	status: 1,
	createDate: 1,
	paidDate: 1,
	currency: 1,
	items: 1,
	allowCancel: 1,	
}

Meteor.publish("Invoice_byId", function (_invoice_id) {
	var invoice_cursor = Invoices.getById_cursor(_invoice_id, {fields: Invoices.publishFields});

	var invoice = invoice_cursor.fetch()[0];
	if (invoice.user_id != this.userId) {
		this.ready();
		return;
	}

	return invoice_cursor;
});

Meteor.publish("Invoices_Paid_byUser", function (_user_id, limit) {
	if (this.userId != _user_id) {
		this.ready();
		return;
	}

	var projection = {fields: Invoices.publishFields};
	if (limit) projection.limit = limit;

	return Invoices.getPaidInvoicesForUser_cursor(_user_id, projection);
});