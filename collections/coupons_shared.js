/**

Coupons
A coupon offers a discount on certain products

coupon: {
	title: 'Half price first month',
	codename: [], // unique codes which this coupon is triggered by
	active: true, // is this coupon being actively used?
	package_id: [], // array of package_ids that this coupon can be used on
	data: {}, // information about how this coupon is applied/what it does for the application to interpret
	statistics: {}, // information about how many times this coupon has been applied etc
}

**/

Coupons = Collections.init("coupons", {
	mixins: [
		Collections.mixins.codename
	]
});

/*****************************************************************************/
/** Collection **/

// Coupons.extendCollection({

// });

/*****************************************************************************/
/** Document **/

Coupons.extendDocument({

});