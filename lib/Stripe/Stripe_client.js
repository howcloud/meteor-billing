if (typeof jQuery !== 'undefined') { // jQuery is now a weak dependency because we don't want to load this in on mobile (ie the only place jQuery is not going to have been forced to be required)
									 // really this is a ui thing => TODO: shift this to meteor-billing-ui package

	var loadStripe = function () {
		
		// might as well use jQuery as we know most of our projects are going to be loading it in anyway
		jQuery.getScript("https://js.stripe.com/v2/", function () {
			Stripe.setPublishableKey(HCConfig.stripePublishableKey);
			
			jQuery('body').trigger('stripe:init');
		});

		// var script = document.createElement('script');
		// script.onload = __callback;
		// script.src = "https://js.stripe.com/v2/";

		// document.getElementsByTagName('head')[0].appendChild(script);

	}

	var loadingStripe = false;
	deferUntilStripeLoaded = function (__callback) {
		if (window.Stripe) {
			__callback();
		} else {
			jQuery('body').one('stripe:init', __callback);
			
			if (!loadingStripe) loadStripe();
		}
	}

}