var stripe;
Meteor.startup(function () {
	stripe = Npm.require('stripe')(HCConfig.stripeSecretKey); // HACK: we assume HCConfig will be defined globally in our main application at some point before startup
});

Stripe = {

	createCustomer: function (_user_id, _data) {
		var response = Async.runSync(function (done) {
			stripe.customers.create(_.extend({
				description: _user_id,
				// card: "tok_14GJTf4ZZPj9v0c2LCh0jk0o" // obtained with Stripe.js, if starting with a card
					   								    // "Passing card will create a new card, make it the new customer default card, and delete the old customer default if one exists"
			}, _data || {}), done);
		});

		if (response.error) return false;
		return response.result;

		/*
		stripe.customers.update("cus_56UDbF9ZY5a8QX", {
			description: "Customer for test@example.com"
		}, function(err, customer) {
			// asynchronously called
		});
		*/
	},

	createCard: function (_customerIdentifier, _cardToken) {
		var response = Async.runSync(function (done) {
			stripe.customers.createCard(_customerIdentifier, {
				card: _cardToken
			}, done);
		});

		if (response.error) return false;
		return response.result;
	},
	deleteCard: function (_customerIdentifier, _cardId) {
		var response = Async.runSync(function (done) {
			stripe.customers.deleteCard(_customerIdentifier, _cardId, done);
		});

		if (response.error) return false;
		return true;
	},
	makeCardDefault: function (_customerIdentifier, _cardId) {
		var response = Async.runSync(function (done) {
			stripe.customers.update(_customerIdentifier, {
				default_card: _cardId
			}, done);
		});

		if (response.error) return false;
		return true;
	},

	doCharge: function (_amount, _card, _customerIdentifier, options) {
		options = options || {};

		var response = Async.runSync(function (done) {
			var data = {
				amount: _amount,
				currency: options.currency ? options.currency : "gbp",
				card: _card, // obtained with Stripe.js [or a stored card identifier]
				customer: _customerIdentifier, // the person who the _card belongs to [required by stripe if we are using a card attached to a customer]
				// capture: true, // whether or not to immediately capture the charge (ie charge it straight away, defaults to true anyway)
			}

			if (options.description) data.description = options.description;
			if (options.statement_description) data.statement_descriptor = options.statement_description;

			stripe.charges.create(data, done);
		});

		if (response.error) return false;
		return response.result;
	},

	upateCustomerData: function (_customerIdentifier, _data) {
		var response = Async.runSync(function (done) {
			stripe.customers.update(_customerIdentifier, _data, done)
		});

		if (response.error) return false;
		return true;
	},

}